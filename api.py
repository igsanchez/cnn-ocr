import os
from flask import Flask, request, redirect, url_for
from flask_restful import Resource, Api
from json import dumps
from werkzeug import secure_filename
import separate
import hashlib

app = Flask(__name__)
api = Api(app)
app.config['UPLOAD_FOLDER'] = 'upload'


class Image(Resource):
    def post(self):

        # check if the post request has the file part
        if 'file' not in request.files:
            return "no file found"
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return "no file selected"
        if file:
            filename = secure_filename(hashlib.md5(file.read()).hexdigest()+"."+file.filename.rsplit('.', 1)[1].lower())
            file.seek(0)
            path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(path)
            path = path.replace("\\","/")
            return separate.seperateCharacters(path)

api.add_resource(Image, '/classify') # Route_1


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)