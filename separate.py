#!/usr/bin/env python
import cv2 as cv
from TFModel2 import TFModel

def seperateCharacters( image_path ):
    label = "retrained_labels.txt"
    model = "retrained_graph.pb"
    tensor = TFModel(model, label, "Placeholder", "final_result")
    # Input File ( Expects one line image of word/sentence )
    img = cv.imread( image_path )
    (h, w) = img.shape[:2]
    image_size = h * w
    image_height = h

    # Set Boundary maker
    mser = cv.MSER_create()
    mser.setMaxArea( int(image_size / 2 ) )
    mser.setMinArea( 300 )

    # Greyscale to increase accuracy
    gray = cv.cvtColor( img, cv.COLOR_BGR2GRAY )
    _, bw = cv.threshold( gray, 0.0, 255.0, cv.THRESH_BINARY | cv.THRESH_OTSU )

    # Create Boundaries
    regions, characters = mser.detectRegions( bw )

    # Sort by x coordinate (k[0]) 
    characters = sorted( characters, key=lambda k: k[0] )
    
    convertedCharacters = list()

    for (x, y, w, h) in characters:
        # ### [DEBUG] View Individual Images
        # cv.imshow( "img",  img[ y:y+h, x:x+w ] )

        # ### [DEBUG] View Whole Image
        # cv.rectangle( img, ( x, y ), ( x + w, image_height ), color=(255, 0, 255), thickness=2) # For Visualization
        # cv.imshow( "img",  img )

        # ### [DEBUG]
        # cv.waitKey() # Press enter in image to cycle through
        

        ### [PROD] Create temporary file, call translator, append value to result
        cv.imwrite("temp.png", img[-image_height :  image_height, x :  x + w] )
        convertedCharacters.append(tensor.classifyImage("temp.png"))

    ### [PROD] Output the result in sentance format
    result = ""
    for ( char ) in convertedCharacters:
        result += char
        print(char)

    return result

### [DEBUG] Test function
# seperateCharacters( "test.jpg" )
