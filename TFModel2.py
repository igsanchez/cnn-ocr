import tensorflow as tf
import numpy as np

class TFModel:
    def __init__(self, modelPath: str, labelPath: str, inputLayer: str, outputLayer: str):
        self.modelFile = modelPath
        self.labelFile = labelPath
        self.inputLayer = inputLayer
        self.outputLayer = outputLayer
        self.inputHeight = 299
        self.inputWidth = 299
        self.inputMean = 0
        self.inputStd = 255
        self.graph = self.loadGraph()
        self.labels = self.loadLabels()
        self.inputNameLayer = "import/" + self.inputLayer
        self.outputNameLayer = "import/" + self.outputLayer
        self.inputOperation = self.graph.get_operation_by_name(self.inputNameLayer)
        self.outputOperation = self.graph.get_operation_by_name(self.outputNameLayer)
        session_conf = tf.ConfigProto(
            intra_op_parallelism_threads=1,
            inter_op_parallelism_threads=1)
        self.sess = tf.Session(graph=self.graph, config=session_conf)

    def loadGraph(self):
        graph = tf.Graph()
        graphDef = tf.GraphDef()

        with open(self.modelFile, "rb") as f:
            graphDef.ParseFromString(f.read())
        with graph.as_default():
            tf.import_graph_def(graphDef)

        return graph

    def readTensor(self, imagePath):
        inputName = "file_reader"
        fileReader = tf.read_file(imagePath)
        # castToFloat = tf.convert_to_tensor(imagePath, dtype=tf.float32)
        imageReader = tf.image.decode_jpeg(
            fileReader, channels=3, name="jpeg_reader")
        castToFloat = tf.cast(imageReader, tf.float32)
        expandDims = tf.expand_dims(castToFloat, 0)
        resized = tf.image.resize_bilinear(expandDims, [self.inputHeight, self.inputWidth])
        normalized = tf.divide(tf.subtract(resized, [self.inputMean]), [self.inputStd])
        sess = tf.Session()
        result = sess.run(normalized)
        return result

    def loadLabels(self):
        labels = []
        asciiLines = tf.gfile.GFile(self.labelFile).readlines()
        for line in asciiLines:
            labels.append(line.rstrip())

        return labels

    def classifyImage(self, imagePath: str):
        tensor = self.readTensor(imagePath)
        results = self.sess.run(self.outputOperation.outputs[0], {
            self.inputOperation.outputs[0]: tensor
        })
        results = np.squeeze(results)
        topResults = results.argsort()[-5:][::-1]

        out = self.labels[topResults[0]]
        if out == "bebi":
            out = "be"

        if out == "bobu":
            out = "bo"

        if out == "dara":
            out = "da"

        if out == "dedi reri":
            out = "de"

        if out == "dodu roru":
            out = "do"

        if out == "ei":
            out = "e"

        if out == "gegi":
            out = "ge"

        if out == "gogu":
            out = "go"

        if out == "hehi":
            out = "he"

        if out == "hohu":
            out = "ho"

        if out == "keki":
            out = "ke"

        if out == "koku":
            out = "ko"

        if out == "leli":
            out = "le"

        if out == "lolu":
            out = "lo"

        if out == "memi":
            out = "me"

        if out == "momu":
            out = "mo"

        if out == "neni":
            out = "ne"

        if out == "ngengi":
            out = "nge"

        if out == "ngongu":
            out = "ngo"

        if out == "nonu":
            out = "no"

        if out == "ou":
            out = "o"

        if out == "pepi":
            out = "pe"

        if out == "popu":
            out = "po"

        if out == "sesi":
            out = "se"

        if out == "sosu":
            out = "so"

        if out == "teti":
            out = "te"

        if out == "totu":
            out = "to"

        if out == "wewi":
            out = "we"

        if out == "wowu":
            out = "wo"

        if out == "yeyi":
            out = "ye"

        if out == "yoyu":
            out = "yo"

        return out
        # for i in topResults:
        # print(self.labels[i], results[i])